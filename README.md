Installation

git clone git@github.com:zoltan-nz/dotfiles.git ~/projects/dotfiles --recurse-submodules
Update submodules

git submodule update --recursive --remote
Symlink the following files:

.bin

.vimrc
.vimrc.bundles
.zshrc
.gemrc

.tmux.conf
.tmux.util
Copy and update:

.zshrc.local